Name:           perl-PkgConfig-LibPkgConf
Version:        0.11
Release:        3
Summary:        Interface to pkg-config files via libpkgconf
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/PkgConfig-LibPkgConf
Source0:        https://cpan.metacpan.org/authors/id/P/PL/PLICEASE/PkgConfig-LibPkgConf-%{version}.tar.gz

Patch0:         PkgConfig-LibPkgConf-0.11-adapt_to_pkgconf_1.9.4.patch
Patch1:         PkgConfig-LibPkgConf-0.11-Fix-resolving-flags-for-packages-with-a-name-differe.patch

BuildRequires:  gcc
BuildRequires:  make
BuildRequires:  perl-devel
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.8.1
BuildRequires:  pkgconfig(libpkgconf) >= 1.5.0
 
%description
Many libraries in compiled languages such as C or C++ provide *.pc files to
specify the flags required for compiling and linking against those libraries.
Traditionally, the command line program pkg-config is used to query these
files. This package provides a Perl-level API using libpkgconf to these files.
 
%prep
%autosetup -p1 -n PkgConfig-LibPkgConf-%{version}
 
%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1 OPTIMIZE="$RPM_OPT_FLAGS"
%{make_build}
 
%install
%{make_install}
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -delete
%{_fixperms} $RPM_BUILD_ROOT/*
 
%check
make test
 
%files
%license LICENSE
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/PkgConfig*
%{_mandir}/man3/*

%changelog
* Thu Aug 3 2023 liyanan <thistleslyn@163.com> - 0.11-3
- Fix perl-alien-build compilation failure caused by pkgconf upgrade to 1.9.5

* Fri Jul 28 2023 dongyuzhen <dongyuzhen@h-partners.com> - 0.11-2
- fix a compilation failure caused by upgrading pkgconf to 1.9.5

* Sat Aug 1 2020 dingyue <dingyue5@huawei.com> - 0.11-1
- Package init
